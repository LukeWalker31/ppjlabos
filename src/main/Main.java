package ppj1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
	Scanner stringC = new Scanner(System.in);
	String linija = stringC.nextLine();
	HashMap<String,String> mapaRegova = new HashMap<>();
	String[] splitanelinije;
	String imeRega;
	String keyStringic;
	char[] polje;
	
	while(!linija.contains("%X")) {
		
		splitanelinije=linija.split(" ");
		imeRega=splitanelinije[0].substring(1, splitanelinije[0].length()-1);
		polje=splitanelinije[1].toCharArray();
		StringBuilder stringB = new StringBuilder();
		
		for(int i=0;i<polje.length;++i) {
	
			if(polje[i]=='\\') {
				stringB.append(polje[i]);
				stringB.append(polje[++i]);
			}
			
			else if(polje[i]=='{') {
				i++;
				StringBuilder stringD = new StringBuilder();
				while(polje[i]!='}') {
					stringD.append(polje[i]);
					++i;
				}
				keyStringic=stringD.toString();
				stringB.append("("+mapaRegova.get(keyStringic)+")");
			} else {
				stringB.append(polje[i]);
			}
			
		}
		
		mapaRegova.put(imeRega, stringB.toString());
		linija=stringC.nextLine();
	}

	
	linija=stringC.nextLine();
	linija=linija.substring(3,linija.length()-1);
	String[] uniZnak = linija.split(" ");
	
	HashMap <String, HashMap<String, ArrayList<String>>> mapaAkcija = new HashMap<>();
	
	while(stringC.hasNextLine()) {
		
		ArrayList<String> listaNaredaba = new ArrayList<>();
		
		linija = stringC.nextLine();
		linija = linija.substring(1);
		
		String[] stanjeiNiz = linija.split(">",2);
		
		polje=stanjeiNiz[1].toCharArray();
		StringBuilder stringB = new StringBuilder();
		
		for(int i=0;i<polje.length;++i) {
	
			if(polje[i]=='\\') {
				stringB.append(polje[i]);
				stringB.append(polje[++i]);
			}
			
			else if(polje[i]=='{') {
				i++;
				StringBuilder stringD = new StringBuilder();
				while(polje[i]!='}') {
					stringD.append(polje[i]);
					++i;
				}
				keyStringic=stringD.toString();
				stringB.append("("+mapaRegova.get(keyStringic)+")");
			} else {
				stringB.append(polje[i]);
			}
			
		}
		
		stanjeiNiz[1]=stringB.toString();
		
		linija=stringC.nextLine();
		while(!linija.equals("}")) {
			if(!linija.equals("{")) {
				listaNaredaba.add(linija);
			}
			
			linija=stringC.nextLine();
		}
		
		if ( mapaAkcija.containsKey(stanjeiNiz[0])) {
			
			mapaAkcija.get(stanjeiNiz[0]).put(stanjeiNiz[1], listaNaredaba);
			
		} else {
			
			mapaAkcija.put(stanjeiNiz[0], new HashMap<String, ArrayList<String>>());
			mapaAkcija.get(stanjeiNiz[0]).put(stanjeiNiz[1], listaNaredaba);
			
		}
	}
	
	stringC.close();
	
	}
}