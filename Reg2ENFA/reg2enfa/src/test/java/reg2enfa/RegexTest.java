package reg2enfa;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import hr.fer.unizg.ppj.enfa.Regex;

class RegexTest {
	
	Regex regex = new Regex();

	@Test
	void test() {
		String izraz = "ab(d(sd)k())";
		int pocetniIndex = 2;
		
		assertEquals(regex.pronadiDesnuZagradu(izraz, pocetniIndex), 11);
	}

}
