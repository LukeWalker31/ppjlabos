package reg2enfa;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import hr.fer.unizg.ppj.enfa.Enfa;

class connectEnfasTest {

	public static Enfa e1 = new Enfa();
	public static Enfa e2 = new Enfa();
	public static Enfa e3 = new Enfa();
	public static Enfa e4 = new Enfa();
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		e1.addState("p1", false);		// The basis ENFA is ready
		
		// The second ENFA
		e2.addState("p2", false);
		e2.addState("p3", true);
		e2.addTransition("p2", Enfa.EPSILON, "p3");
		
		// The third ENFA
		Map<String, Boolean> states = new HashMap<>();
		states.put("p1", false);
		states.put("p2", false);
		states.put("p3", false);
		states.put("p4", true);
		states.put("p5", false);
		e3.addStates(states);
		e3.setStartingState("p1");
		
		// Setting the alphabet
		e3.addAlphabetChar("a", "b");
		
		// Setting the transmissions
		Map<String, String[]> transMap = new HashMap<>();		//Map for State p1
		transMap.put("a", new String[]{"p3"});
		transMap.put("b", new String[]{"p2"});
		e3.addTransitions("p1", transMap);
		e3.addTransition("p2", Enfa.EPSILON, new String[] {"p4"});
		e3.addTransition("p3", Enfa.EPSILON, new String[] {"p5"});
		
		// The fourth ENFA
		// Adding states
		states = new HashMap<>();
		e4.addState("p1", false);
		e4.setStartingState("p1");		// Let's set the starting State
		
		states.put("p2", false);
		states.put("p3", false);
		states.put("p4", true);
		e4.addStates(states);
		
		// Setting the alphabet
		e4.addAlphabetChar("a", "b");
		
		
		// Setting the transmissions
		transMap = new HashMap<>();
		
		// Map for p1
		transMap.put("a", new String[] {"p2"});
		transMap.put(Enfa.EPSILON, new String[] {"p4"});
		e4.addTransitions("p1", transMap);	// For p1
		
		// Map for p2
		transMap.put("a", new String[]{"p2"});
		transMap.put("b", new String[]{"p3"});
		e4.addTransitions("p2", transMap);
		
		// Map for p3
		transMap.clear();
		transMap.put("a", new String[]{"p3"});
		transMap.put(Enfa.EPSILON, new String[]{"p4"});
		e4.addTransitions("p3", transMap);
		
		// p4 has got no transitions - end-state
		
		// Prepare the Automaton for operation
		e4.prepare();
		
	}

	@BeforeEach
	void setUp() throws Exception {
		// Not yet needed
	}

	@Test
	void connectEnfasMethodTest() {
		e1
	}

}
