package hr.fer.unizg.ppj.enfa;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

import hr.fer.unizg.ppj.enfa.Enfa.Automaton.State;

/*
Example program:

a,b,c,b,a,d
s1,s2,s3,s4,s5,s6,s7
a,b,c,d
s2,s5
s1
s1,$->s3,s6,s7
s1,a->s1,s3
s1,b->s5
s1,c->s3
s1,d->s3
s2,$->s2
s2,a->s3
s2,b->s1,s5
s2,c->s2,s2
s2,d->s4static 
s3,$->s3
s3,a->#
s3,b->s2,s3
s3,c->s4,s5
s3,d->s1
s4,a->s4
s4,b->s1,s4
s4,c->s1,s5
s5,a->s1
s5,b->s5
*/

/**
 * A public API towards the inner Automaton mechanisms.
 * 
 * Before using the Enfa, the method prepare() MUST be called!
 * 
 * @author kikyy99
 */
public class Enfa {
	/**
	 * Epsilon transition - used to reach acceptable states at the end of input
	 */
	public static final String EPSILON = "$";
	private static final AlphabetChar EPSILON_TRANSITION = new AlphabetChar(EPSILON);
	public static final String EMPTY_STATE_STRING = "#";
	
	/**
	 * The inner, wrapped Automaton.
	 */
	private Automaton automaton;
	/**
	 * The index of the input seq. character that YET NEEDS TO BE PROCESSED
	 */
	private int nextIndex;

	/**
	 * Connects this Automaton to another Automaton. This is done by providing an Epsilon-transition
	 * from this Automaton's starting state to the starting state of the arg Automaton.
	 * 
	 * The calling Automaton shall practically incorporate a copy of the other Automaton's data
	 * into itself(the calling Automaton) and thus it shall be changed. The other Automaton will
	 * remain intact and unchanged.
	 * 
	 * Note that by doing this the other Automaton's input is not carried over to this Automaton.
	 */
	public void connectEnfas(Enfa other) {
		//let's make an epsilon transition and connect the 2 automatons using it
		Objects.requireNonNull(other);
		
		/* Need to move all the data from the other Automaton to this Automaton before
		 * connecting their starting States.
		 */
		this.automaton.addAlphabetSet(other.getAlphabet());
		this.automaton.addStatesSet(other.getStates());
		
		Automaton.State startingStateFirst = this.getStartingState();
		Automaton.State startingStateSecond = other.getStartingState();
		startingStateFirst.addTransition(EPSILON_TRANSITION.toString(), new String[] {startingStateSecond.toString()});
	}
	
	/**
	 * Initializes an empty Automaton - its yet needs to be set up and filled.
	 */
	public Enfa() {
		this.automaton = new Automaton();
	}
	
	/**
	 * Add single State
	 * 
	 * @param name The name of the state
	 * @param acceptedState Whether this state is acceptable or not
	 */
	public void addState(String name, boolean acceptedState) {
		this.automaton.addState(name, acceptedState);
	}
	
	/**
	 * Adds multiple states into this Automaton
	 * 
	 * @param states The Map consisting of stateName-stateAcceptability key-value pairs. Must not be null.
	 */
	public void addStates(Map<String, Boolean> states) {
		Objects.requireNonNull(states);
		
		for(Map.Entry<String, Boolean> state : states.entrySet()) {
			String name = state.getKey();
			Boolean acceptedState = state.getValue();
			this.automaton.addState(name, acceptedState);
		}
	}
	
	/**
	 * Sets the starting state of the Automaton.
	 * 
	 * @param name The name of the state that shall be set as the starting state. Must not be null.
	 */
	public void setStartingState(String name) {
		this.automaton.setStartingState(name);
	}
	
	/**
	 * Prepares the Automaton for execution. This method
	 * MUST be called.
	 * 
	 * After this method the Automaton is ready for operation.
	 */
	public void prepare() {
		this.automaton.activeStates = this.automaton.activeStatesWithEpsilonEnv();
	}
	
	/**
	 * Returns the starting state. Useful to the connectEnfas function.
	 * 
	 * @return The starting State of the Automaton.
	 */
	public Automaton.State getStartingState() {
		return this.automaton.getStartingState();
	}
	
	/**
	 * Adds input alphabet characters to the Automaton
	 * 
	 * @param value
	 */
	public void addAlphabetChar(String first, String...others) {
		this.automaton.addAlphabetChars(first, others);
	}
	
	/**
	 * Sets the input of the Automaton.
	 * 
	 * @param input The input of this Automaton. Must not be null.
	 */
	public void setInput(List<AlphabetChar> input) {
		this.automaton.setInput(input);
	}
	
	/**
	 * @return The alphabet set of the Automaton.
	 */
	public Set<AlphabetChar> getAlphabet() {
		return this.automaton.getAlphabet();
	}
	
	/**
	 * @return the states set of the Automaton
	 */
	public Set<Automaton.State> getStates() {
		return this.automaton.states;
	}
	
	/**
	 * Appends a single character to the input.
	 * 
	 * @param inputChar The character to be appended to the input. Must not be null.
	 */
	public void appendInput(AlphabetChar inputChar) {
		this.automaton.appendInput(inputChar);
	}
	
	/**
	 * Adds a SINGLE transition.
	 * 
	 * @param stateName The source State.
	 * @param alphabetChar The transition character.
	 * @param destinationStates The destination States array.
	 */
	public void addTransition(String stateName, String alphabetChar, String[] destinationStates) {
		this.automaton.bindTransition(stateName, alphabetChar, destinationStates);
	}
	
	/**
	 * Adds a SINGLE Transition.
	 * 
	 * @param stateName The source State.
	 * @param alphabetChar	The transition character.
	 * @param destinationState	The destination State
	 */
	public void addTransition(String stateName, String alphabetChar, String destinationState) {
		this.automaton.bindTransition(stateName, alphabetChar, destinationState);
	}
	
	/**
	 * Adds a SINGLE EPSILON Transition.
	 * 
	 * @param stateName The source State.
	 * @param destinationState The destination State
	 */
	public void addEpsilonTransition(String stateName, String destinationState) {
		this.automaton.bindTransition(stateName, Enfa.EPSILON, destinationState);
	}
	
	/**
	 * Adds MULTIPLE transitions.
	 * 
	 * @param stateName The source State.
	 * @param transitions A map consisting of alphabetChar - destinationStates key-value pairs.
	 * AlphabetCharacters are transition characters, and desinationStates are destination State arrays.
	 */
	public void addTransitions(String stateName, Map<String, String[]> transitions) {
		this.automaton.bindTransitions(stateName, transitions);
	}
	
	/**
	 * Sets this as input for the automaton.
	 * 
	 * @param inputSeqs An array of strings consisting of input elements
	 */
	public void setInput(String[] inputSeqs) {
		Objects.requireNonNull(inputSeqs);
		
		List<AlphabetChar> alphCharList = new ArrayList<>();
		
		for(String current : inputSeqs) {
			AlphabetChar alphChar = new AlphabetChar(current);
			alphCharList.add(alphChar);
		}
		
		this.automaton.setInput(alphCharList);
	}
	
	/**
	 * Appends one element at the end of the input sequence
	 * 
	 * @param inputChar the input character to be appended
	 */
	public void appendInputCharacer(String inputChar) {
		Objects.requireNonNull(inputChar);
		
		AlphabetChar toAppend = new AlphabetChar(inputChar);
		this.automaton.appendInput(toAppend);
	}
	
	/**
	 * Performs transition for a single(the next) input seq character
	 * 
	 * @return false if the whole input sequence has been processed, true otherwise
	 */
	public boolean performSingleTransition() {
		return this.automaton.readInputChar(nextIndex++);
	}
	
	/**
	 * Performs transition for the specified alphabet character
	 * 
	 * @param alphChar The alphabet character for which the transmission shall be performed
	 */
	public void performSingleTransition(String alphChar) {
		this.automaton.readInputChar(new AlphabetChar(alphChar));
	}
	
	/**
	 * Performs transitions for the whole input sequence
	 * 
	 * @return true whether the Enfa finished in an acceptable State, false otherwise
	 */
	public boolean performWholeSequenceTransition() {
		for(int i = 0; i < this.automaton.input.size(); i++) {
			this.automaton.readInputChar(i);
		}
		
		// Check whether any State is acceptable
		for(State currentState : this.automaton.activeStates) {
			if(currentState.acceptedState) {
				return true;	// An acceptable state was found among active states
			}
		}
		return false;	// No acceptable state found among active states
	}
	
	/**
	 * @return whether the Enfa is currently located in an acceptable State or not
	 */
	public boolean isAccepted() {
		// Need to iterate through all the active States
		for(State currState : this.automaton.getActiveStates()) {
			if(currState.acceptedState) {
				return true;			// An acceptable State has been found
			}
		}
		
		return false;		// An acceptable State hasn't been found
	}
	
	/**
	 * @return the currently active states of the Automaton
	 */
	public Set<State> getActiveStates() {
		return this.automaton.activeStates;
	}
	
	/**
	 * Used for testing purposes - not recommended otherwise
	 * 
	 * @return the empty state of the Automaton
	 */
	public State getEmptyState() {
		return this.automaton.EMPTY_STATE;
	}
	
	/**
	 * Returns the number of states this Automaton has
	 * 
	 * @return the number of states
	 */
	public int getStatesLength() {
		return this.automaton.states.size();
	}
	
	/**
	 * Class that models a single input alphabet character.
	 * 
	 * @author kikyy99
	 *
	 */
	public static class AlphabetChar implements Comparable<AlphabetChar> {
		
		private String value;

		public AlphabetChar(String value) {
			Objects.requireNonNull(value);
			
			this.value = value;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			AlphabetChar other = (AlphabetChar) obj;
			if (value == null) {
				if (other.value != null)
					return false;
			} else if (!value.equals(other.value))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return value;
		}

		@Override
		public int compareTo(AlphabetChar o) {
			//shall compare them by String natural ordering
			return this.value.compareTo(o.value);
		}
		
	}
	
	/**
	 * Features the automaton's states. Those states feature
	 * their own transitions stored internally. Is an internal class
	 * encapsulated/wrapped by the Enfa class.
	 * 
	 * @author kikyy99
	 *
	 */
	public class Automaton {
		
		/**
		 * Default empty state
		 */
		private State EMPTY_STATE = new State("#", false);
		private Set<State> states =  new TreeSet<>();
		private Set<State> visitedStates = new LinkedHashSet<>();
		private State startingState;
		private Set<State> activeStates = new TreeSet<>();
		private Set<AlphabetChar> alphabet = new LinkedHashSet<>();
		private List<AlphabetChar> input = new ArrayList<>();
		
		/**
		 * A constructor which is provided an alphabet
		 * 
		 * @param alphabet The input string characters
		 */
		private Automaton(Set<AlphabetChar> alphabet) {
			this();
			Objects.requireNonNull(alphabet);
			this.alphabet.addAll(alphabet);
		}
		
		/**
		 * An empty constructor - this Automaton yet needs to be set up!
		 */
		private Automaton() {
			this.states.add(EMPTY_STATE);
			this.alphabet.add(EPSILON_TRANSITION);
		}
		
		/**
		 * Adds the alphabet character to the Automaton. 
		 * 
		 * @param first The mandatory arg - the character to be added. Must not be null.
		 * @param others Optional arg - other characters to be added as well
		 */
		private void addAlphabetChars(String first, String...others) {
			Objects.requireNonNull(first);
			AlphabetChar toAdd = new AlphabetChar(first);
			alphabet.add(toAdd);
			
			for(String single : others) {
				toAdd = new AlphabetChar(single);
				alphabet.add(toAdd);
			}
		}
		
		/**
		 * Directly adds to the entire Automaton's alphabet.
		 * 
		 * @param alphabet The set of AlphabetChars that shall be used
		 */
		private void addAlphabetSet(Set<AlphabetChar> alphabet) {
			Objects.requireNonNull(alphabet);
			this.alphabet.addAll(alphabet);
		}
		
		private State getState(String find) {
			Objects.requireNonNull(find);
			if(find.equals(EMPTY_STATE_STRING)) {
				return EMPTY_STATE;
			}
			
			for(State currState : states) {
				if(currState.name.equals(find)) {
					return currState;
				}
			}
			
			return null;
		}
		
		/**
		 * The state that shall be set as the starting state. Note that first
		 * this state MUST BE ADDED TO THE AUTOMATON, and only after it is present
		 * in the Automaton can it be set as the starting State.
		 * 
		 * @param name The name of the state to be set as the starting State
		 */
		public void setStartingState(String name) {
			Objects.requireNonNull(name);
			
			State startingState = this.getState(name);
			this.startingState = startingState;
			activeStates.add(startingState);
			
			// Disregard the comment below, making some changes
//			 But that's not all - need to take into consideration the Epsilon-environment as well
//			activeStates = this.activeStatesWithEpsilonEnv();
		}
		
		/**
		 * @return the startingState
		 */
		public State getStartingState() {
			return startingState;
		}

		/**
		 * Sets the input of the Automaton.
		 * 
		 * @param input The list of AlphabetChars to be set as input.
		 */
		private void setInput(List<AlphabetChar> input) {
			Objects.requireNonNull(input);
			this.input.addAll(input);
		}
		
		/**
		 * Appends a single character to the input sequence.
		 * 
		 * @param inputChar The input character to be appended
		 */
		private void appendInput(AlphabetChar inputChar) {
			Objects.requireNonNull(inputChar);
			this.input.add(inputChar);
		}
		
		/**
		 * Adds a single state
		 * 
		 * @param name
		 * @param acceptedState
		 */
		private void addState(String name, boolean acceptedState) {
			State toAdd = new State(name, acceptedState);
			states.add(toAdd);
		}
		
		/**
		 * Adds multiple states.
		 * 
		 * @param statesSet The states set which multiple states shall be drawn from
		 */
		private void addStatesSet(Set<State> statesSet) {
			Objects.requireNonNull(statesSet);
			this.states.addAll(statesSet);
		}
		
		/**
		 * Internal function that binds MULTIPLE transitions to the Automaton
		 * 
		 * @param stateName the name of the source state
		 * @param transitions A map consisting of String-String[] key-value pairs. The first part
		 * - the String is the name of the alphabet character, and the second part - the String[]
		 * is the array of destination States.
		 */
		private void bindTransitions(String stateName, Map<String, String[]> transitions) {
			Objects.requireNonNull(stateName);
			Objects.requireNonNull(transitions);
			
			State state = this.getState(stateName);
			state.addTransitions(transitions);
		}
		
		/**
		 * Internal function that binds a SINGLE transition to the Automaton
		 * 
		 * @param stateName the name of the source state
		 * @param alphChar the alphabet character for which the transition shall be performed
		 * @param destinationStates self-explanatory
		 */
		private void bindTransition(String stateName, String alphChar, String[] destinationStates) {
			Objects.requireNonNull(stateName);
			Objects.requireNonNull(alphChar);
			Objects.requireNonNull(destinationStates);
			
			State state = this.getState(stateName);
			state.addTransition(alphChar, destinationStates);
		}
		
		/**
		 * Internal function that binds a SINGLE transition to the Automaton
		 * 
		 * @param stateName the name of the source state
		 * @param alphChar the alphabet character for which the transition shall be performed
		 * @param destinationState self-explanatory
		 */
		private void bindTransition(String stateName, String alphChar, String destinationState) {
			Objects.requireNonNull(stateName);
			Objects.requireNonNull(alphChar);
			Objects.requireNonNull(destinationState);
			
			State state = this.getState(stateName);
			state.addTransition(alphChar, destinationState);
		}
		
//		private void addStates(Map<String, List<Boolean, Map<String, State[]>>> statesMap) {
//			for(Entry<String, Boolean> entry : statesMap.entrySet()) {
//				String name = entry.getKey();
//				Boolean acceptedState= entry.getValue();
//				
//				this.addState(name, acceptedState);
//			}
//		}
		
		/**
		 * @return false if no more input chars left to be processed,
		 * true otherwise
		 */
		private void readInputSequenceWResPrint() {
			StringBuilder sb = new StringBuilder();
			sb.append(activeStatesWithEpsilonEnv());
			for(int i = 0; i < input.size(); i++) {
				sb.append("|");
				readInputChar(i);
				if(activeStates.size()>=2) {
					//in case an empty state still made it into the set,
					//but there are more than 2 states in the set itself
					activeStates.remove(EMPTY_STATE);
				}
				sb.append(activeStatesWithEpsilonEnv());
			}
			
			String toPrint = sb.toString();
			toPrint = toPrint.replace("[", "");
			toPrint = toPrint.replace("]", "");
			toPrint = toPrint.replaceAll("\\s*", "");
			System.out.println(toPrint);
		}
		
		/**
		 * Calculates the set consisting of currently active states and their
		 * recursively-combined Epsilon environment States.
		 * 
		 * Note that this method
		 * doesn't modify the activeStates private field in any way, merely returns a new
		 * Set.
		 * 
		 * Should you wish to further modify the activeStates, such an action needs to be done manually
		 * by yourself.
		 * 
		 * @return the new set consisting of currently active states and their
		 * recursively-combined Epsilon environment States
		 */
		private Set<State> activeStatesWithEpsilonEnv() {
			Set<State> activePlusEpsilon = new TreeSet<>();
			
			for(State currentState : activeStates) {
				activePlusEpsilon.addAll(currentState.epsilonEnvironment());
			}
			
			if(activePlusEpsilon.size()>=2) {
				// if there is an empty state here, IT IS NOT ALONE, SO REMOVE IT
				activePlusEpsilon.remove(EMPTY_STATE);
			}
			
			return activePlusEpsilon;
		}
		
		/**
		 * Performs a transition of the Automaton for a single input character.
		 * 
		 * @param nextIndex the index of the next sequence character to be processed
		 * @return false if all the input has been processed, true otherwise(aka if a transition has been made)
		 */
		private boolean readInputChar(int nextIndex) {
			if(startingState == null) {
				throw new NullPointerException("Starting state not set!");
			}
			
			if(nextIndex >= input.size()) {
				return false;
			}
			
			AlphabetChar nextChar = this.input.get(nextIndex);
			readInputChar(nextChar);
			return true;
		}
		
		/**
		 * Performs a transition of the Automaton for a single input character.
		 * 
		 * @param alphChar the alphabet character for which a transmission shall be performed
		 */
		private void readInputChar(AlphabetChar alphChar) {
			if(startingState == null) {
				throw new NullPointerException("Starting state not set!");
			}
			
			Set<State> epsilonEnv = new HashSet<>();
			
			//get the epsilon-env of all the former states
			for(State state : activeStates) {
				epsilonEnv.addAll(state.epsilonEnvironment());
			}

			//first remove all current states
			activeStates.clear();
			//perform transitions for ALL the states located in the epsilon env
			for(State state : epsilonEnv) {
				state.performTransition(alphChar);
			}
			
			//now find the epsilon environment of all the newfound active states, mark them all as active
			Set<State> activeWithEpsilons = new HashSet<>(activeStates);
			for(State state : activeWithEpsilons) {
				activeStates.addAll(state.epsilonEnvironment());
			}
		}
		
		public void reset() {
			this.activeStates.clear();
			this.activeStates.add(startingState);
			this.input.clear();
		}
		
//		public String visitedStates() {
//			StringBuilder sb = new StringBuilder();
//			
//			sb.append(visitedStates);
//			sb.append("|");
//			
//			visitedStates.clear();
//			return sb.toString();
//		}
		
		/**
		 * Internal class modelling a single State.
		 * @author kikyy99
		 *
		 */
		public class State implements Comparable<State>{
			
			private String name;
			private final boolean acceptedState;
			private Set<Transition> transitions = new HashSet<>();
			private boolean active;
			private AlphabetChar lastCharacter;
			
			private State(String name, boolean acceptedState, Transition...transitions) {
				Objects.requireNonNull(name);		//State MUST have a name, and it must be UNIQUE
				Objects.requireNonNull(acceptedState);
				
				this.name = name;
				this.acceptedState = acceptedState;

				//we don't have any transitions from this state to others
				if(transitions == null) {
					return;
				}
				
				//else add all the transitions to our transitions set
				for(Transition currentTransition : transitions) {
					this.transitions.add(currentTransition);
				}
			}
			
			private State(String name, boolean acceptedState) {
				this(name, acceptedState, (Transition[])null);
			}
			
			public void addTransition(String input, String[] destinationStates) {
				Objects.requireNonNull(destinationStates);
				
				Transition toAdd = new Transition(input, destinationStates); 
				transitions.add(toAdd);
			}
			
			public void addTransition(String input, String destinationState) {
				Objects.requireNonNull(destinationState);
				
				Transition toAdd = new Transition(input, destinationState); 
				transitions.add(toAdd);
			}
			
			public void addTransitions(Map<String, String[]> transitions) {
				Objects.requireNonNull(transitions);
				
				for(Entry<String, String[]> entry : transitions.entrySet()) {
					String input = entry.getKey();
					String[] states = entry.getValue();
					
					Objects.requireNonNull(input);
					Objects.requireNonNull(states);
					if(states.length == 0) {
						throw new IllegalArgumentException("Array of size 0 passed as argument to the "
								+ "addTransitions method!");
					}
					
					this.addTransition(input, states);
				}
			}
			
			/**
			 * Finds the recursively-combined Set featuring the epsilon environment States
			 * of the current State
			 * 
			 * @return recursively-combined Set featuring the epsilon environment States
			 * of the current State
			 */
			private Set<State> epsilonEnvironment(){
				Set<State> epsilonEnvironment = new HashSet<>();
				epsilonEnvironment.add(this);
				//let's check if this contains any epsilon transitions, and if it does
				//add those states to the epsilon env. Repeat recursively
				
				//recursive checking - do it until the two sets are equal
				Set<State> newEpsilonEnv = new HashSet<>(epsilonEnvironment);
				boolean equals;
				do {
					equals = true;
					for(State state : epsilonEnvironment) {
						for(Transition transition : state.transitions) {
							if(transition.input.equals(EPSILON_TRANSITION)) {
								equals = false;
								newEpsilonEnv.addAll(transition.targetStates);
								visitedStates.addAll(transition.targetStates);
							}
						}
					}
					
					if(newEpsilonEnv.equals(epsilonEnvironment)) {
						break;
					}
					epsilonEnvironment.addAll(newEpsilonEnv);
				} while(!equals);

				return epsilonEnvironment;
			}
			
			/**
			 * Performs a transition
			 * 
			 * @param currentChar the input alphabet char
			 * @return true if transition has been found, false otherwise
			 */
			private boolean performTransition(AlphabetChar currentChar) {
				boolean matches = false;
				Transition matchedTrans = null;
				for(State.Transition currentTransition : transitions) {
					if(currentTransition.input.equals(currentChar)) {
						matches = true;
						matchedTrans = currentTransition;
						break;
					}
				}

				this.active = false;
				this.lastCharacter = currentChar;
				//okay, time to do the algo part
				if(matchedTrans == null && !activeStates.isEmpty()) {
					//one of states went into nothing - nothing too bad
					return false;
				}
				
				if(!matches) {
					//not matched and no active states, go to empty state.
					activeStates.add(Automaton.this.EMPTY_STATE);
					Automaton.this.EMPTY_STATE.active = false;
					Automaton.this.visitedStates.add(EMPTY_STATE);
					return false;
				}
				
				//else we have a normal state
				Set<State> nextStates = matchedTrans.targetStates;
				for(State currentState : nextStates) {
					currentState.active = true;
					visitedStates.add(currentState);
					activeStates.add(currentState);
				}
				
				//that's it. 
				return true;
			}
			
			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result + ((name == null) ? 0 : name.hashCode());
				return result;
			}

			@Override
			public boolean equals(Object obj) {
				if (this == obj)
					return true;
				if (obj == null)
					return false;
				if (getClass() != obj.getClass())
					return false;
				State other = (State) obj;
				if (name == null) {
					if (other.name != null)
						return false;
				} else if (!name.equals(other.name))
					return false;
				return true;
			}
			
			@Override
			public int compareTo(State o) {
				return this.name.compareTo(o.name);
			}
			
			@Override
			public String toString() {
				return name;
			}

			/**
			 * Models a SINGLE transition - therefore it MUST have an input, 
			 * and it MUST have at least one target state. It SHALL not be used
			 * to model transitions to the {@link EMPTY_STATE} or model transitions
			 * that have no input(input is null). 
			 * 
			 * @author kikyy99
			 *
			 */
			private class Transition {
				
				/**
				 * The input character
				 */
				private AlphabetChar input;
				/**
				 * ALL THE TARGET STATES - they MUST be UNIQUE
				 */
				private Set<State> targetStates = new HashSet<>();
				
				/**
				 * Constructor.
				 * @param input
				 * @param targetStates
				 */
				private Transition(String input, String... targetStates) {
					Objects.requireNonNull(input);
					Objects.requireNonNull(targetStates);

					this.input = new AlphabetChar(input);
					//add all the elements from our array to this set
					for(String currentState : targetStates) {
						//search the automaton for the wanted state;
						State toAdd = Automaton.this.getState(currentState);
						this.targetStates.add(toAdd);
					}
				}

				@Override
				public int hashCode() {
					final int prime = 31;
					int result = 1;
					result = prime * result + ((input == null) ? 0 : input.hashCode());
					result = prime * result + ((targetStates == null) ? 0 : targetStates.hashCode());
					return result;
				}

				@Override
				public boolean equals(Object obj) {
					if (this == obj)
						return true;
					if (obj == null)
						return false;
					if (getClass() != obj.getClass())
						return false;
					Transition other = (Transition) obj;
					if (input == null) {
						if (other.input != null)
							return false;
					} else if (!input.equals(other.input))
						return false;
					if (targetStates == null) {
						if (other.targetStates != null)
							return false;
					} else if (!targetStates.equals(other.targetStates))
						return false;
					return true;
				}
				
				@Override
				public String toString() {
					String toReturn = String.format("&(%s, %s) = %s", State.this, input, targetStates);
					toReturn.replace('[', '{');
					toReturn.replace(']', '}');
					
					return toReturn;
				}
			}
		}

		/**
		 * @return the alphabet
		 */
		public Set<AlphabetChar> getAlphabet() {
			return alphabet;
		}

		/**
		 * @return the activeStates
		 */
		public Set<State> getActiveStates() {
			return activeStates;
		}
	}
	
	/**
	 * Features some data about States. Made to help with the mainFormer function when
	 * parsing input from a file and forming a new Automaton. May yet be of help - not sure?
	 * 
	 * @author kikyy99
	 *
	 */
	// TODO: check whether this class is needed/useful
	public static class StateData {
		
		private String name;
		public boolean acceptable;
		public Map<String, String[]> transitions;
		
		public StateData(String name, boolean acceptable, Map<String, String[]> transitions) {
			super();
			this.name = name;
			this.acceptable = acceptable;
			this.transitions = transitions;
		}
	}

	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(automaton);
	}

	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Enfa)) {
			return false;
		}
		Enfa other = (Enfa) obj;
		return Objects.equals(automaton, other.automaton);
	}
}