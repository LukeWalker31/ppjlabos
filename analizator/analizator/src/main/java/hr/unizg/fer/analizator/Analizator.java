package hr.unizg.fer.analizator;

import java.util.List;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import hr.fer.unizg.ppj.enfa.Enfa;
import hr.fer.unizg.ppj.enfa.Enfa.AlphabetChar;
import hr.fer.unizg.ppj.enfa.Enfa.StateData;

public class Analizator {
	public static void mainFormer(String[] args) throws IOException, InterruptedException {
		//let's get parsing
		List<List<String>> inputSequences = null;
		Set<String> states;
		Set<String> alphabet;
		Set<String> acceptableStates;
		String startState;
		Set<String> transitions;
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        
		//second row - states set
		String input = reader.readLine();
		String[] parts = input.split(",");
		states = new LinkedHashSet<String>(parts.length);
		for(String part : parts) {
			part = part.trim();
			states.add(part);
		}
		
		//third row - alphabets set
		input = reader.readLine();
		parts = input.split(",");
		alphabet = new LinkedHashSet<String>(parts.length);
		for(String part : parts) {
			part = part.trim();
			alphabet.add(part);
		}
		
		//fourth row -  acceptable states set
		input = reader.readLine();
		parts = input.split(",");
		acceptableStates = new LinkedHashSet<String>(parts.length);
		for(String part : parts) {
			part = part.trim();
			acceptableStates.add(part);
		}
		
		//fifth row - starting state
		startState = reader.readLine();
		startState = startState.trim();
		states.add(startState);
		
		//sixth row+ - transitions
		//TODO: kasnije promijeni ovaj dio
		transitions = new LinkedHashSet<>();

//		input = reader.readLine();
//		while(!input.equals("kraj")) {
//			input = input.trim();
//			transitions.add(input);
//			input = reader.readLine();
//		}
		
		input = reader.readLine();
		while(input != null) {
			input = input.trim();
			transitions.add(input);
//			Thread.sleep(10);
			input = reader.readLine();
		}
		
		Enfa ourAutomaton = new Enfa();
		
		
		//Let's construct an automaton!

		for(String alphabetCharacter : alphabet) {
			ourAutomaton.addAlphabetChar(alphabetCharacter);;
		}
		
		
		//Make the StateData array featuring data about all the states
		Map<String, StateData> statesData = new HashMap<String, StateData>();
		for(String currentState : states) {
			//let's see if it's acceptable
			boolean acceptable = false;
			if(acceptableStates.contains(currentState)) {
				acceptable = true;
			}
			
			//need to go through all the transitions, and make the map
			Map<String, String[]> transitionsMap = new HashMap<>();
			for(String transition : transitions) {
				transition.replaceAll("\\s", "");
				parts = transition.split("->");
				String[] subPartsLeft = parts[0].split(",");
				
				String stateName = subPartsLeft[0];			//the name of the state - check if it equals to the currentState
				if(stateName.equals(currentState)) {
					//get the character -> alfChar and the destination states
					String character = subPartsLeft[1];
					String[] destinationStates = parts[1].split(",");
					transitionsMap.put(character, destinationStates);
				}
			}
			
			//that' it - we have a piece of data --> add it to the set
			StateData toAdd = new StateData(currentState, acceptable, transitionsMap);
			statesData.put(currentState, toAdd);
		}
		
		//let's add transitions and states to the automaton!
		for(Entry<String, StateData> entry : statesData.entrySet()) {
			String stateName = entry.getKey();
			StateData data = entry.getValue();
			
			ourAutomaton.addState(stateName, data.acceptable);
		}
		
		for(Entry<String, StateData> entry : statesData.entrySet()) {
			String stateName = entry.getKey();
			StateData data = entry.getValue();
			
			ourAutomaton.addTransitions(stateName, data.transitions);
		}
		
		ourAutomaton.setStartingState(startState);
		
		//let's try reading the input... ughhh
		List<AlphabetChar> automatonInput = new ArrayList<>();
		for(List<String> inputSequence : inputSequences) {
			for(String alphChar : inputSequence) {
				AlphabetChar propAlphChar = new AlphabetChar(alphChar);
				automatonInput.add(propAlphChar);
			}
			
			//once all the chars are in - pass to automaton, execute the reading, and then clear the input
			//for another round!
			ourAutomaton.setInput(automatonInput);
			ourAutomaton.performWholeSequenceTransition();
			automatonInput.clear();
			//ourAutomaton.reset();
		}
	}

}
